<?php
namespace ResourceCatalog\Model;

interface IUserGroup extends IUserGroupIdentify
{
    const ID = array(
        'NULL' => 0,
        'FGW' => 1,
        'SSCJDGLJ' => 2,
        'SSFJ' => 3,
        'SWSJKHTYJ' => 4,
        'SSWJ' => 5,
        'STHJJHZSFJ' => 6,
        'SWJ' => 7,
        'SGAJ' => 8,
        'SRSJ' => 9,
        'SCZJ' => 10,
        'SMZJ' => 11,
        'SGYHXXHJ' => 12,
        'SJKJ' => 13,
        'SJTYSJ' => 14,
        'STJJ' => 15,
        'SYLBZJ' => 16,
        'SWHHLYJ' => 17,
        'SYJGLJ' => 18,
        'SSLJ' => 19,
        'SSJJ' => 20,
        'SNYNCJ' => 21,
        'SZJJ' => 22,
        'SZRZYJ' => 23,
        'SXFJYDD' => 24,
        'SCGJ' => 25,
        'SXZSPFWGLJ' => 26,
    );
}
