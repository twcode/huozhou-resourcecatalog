<?php
namespace ResourceCatalog\Model;

interface IUserGroupIdentify
{
    const IDENTIFY = array(
        IUserGroup::ID['NULL'] =>'NULL' ,
        IUserGroup::ID['FGW'] =>'FGW' ,
        IUserGroup::ID['SSCJDGLJ'] =>'SSCJDGLJ' ,
        IUserGroup::ID['SSFJ']=>'SSFJ' ,
        IUserGroup::ID['SWSJKHTYJ'] =>'SWSJKHTYJ' ,
        IUserGroup::ID['SSWJ'] =>'SSWJ' ,
        IUserGroup::ID['STHJJHZSFJ']=>'STHJJHZSFJ',
        IUserGroup::ID['SWJ']=>'SWJ',
        IUserGroup::ID['SGAJ']=>'SGAJ',
        IUserGroup::ID['SRSJ']=>'SRSJ',
        IUserGroup::ID['SCZJ']=>'SCZJ',
        IUserGroup::ID['SMZJ']=>'SMZJ',
        IUserGroup::ID['SGYHXXHJ']=>'SGYHXXHJ',
        IUserGroup::ID['SJKJ']=>'SJKJ',
        IUserGroup::ID['SJTYSJ']=>'SJTYSJ',
        IUserGroup::ID['STJJ']=>'STJJ',
        IUserGroup::ID['SYLBZJ'] =>'SYLBZJ' ,
        IUserGroup::ID['SWHHLYJ'] =>'SWHHLYJ' ,
        IUserGroup::ID['SYJGLJ'] =>'SYJGLJ' ,
        IUserGroup::ID['SSLJ']=>'SSLJ' ,
        IUserGroup::ID['SSJJ']=>'SSJJ',
        IUserGroup::ID['SNYNCJ'] =>'SNYNCJ' ,
        IUserGroup::ID['SZJJ'] =>'SZJJ' ,
        IUserGroup::ID['SZRZYJ'] =>'SZRZYJ' ,
        IUserGroup::ID['SXFJYDD'] =>'SXFJYDD' ,
        IUserGroup::ID['SCGJ']=>'SCGJ',
        IUserGroup::ID['SXZSPFWGLJ']=>'SXZSPFWGLJ',
    );
}
