### 目录

* [简介](#abstract)
* [版本记录](#version)

---

### <a name="abstract">简介</a>

用于山西-霍州资源目录信息配置

---

### <a name="version">版本记录</a>

* [1.0.0](./Docs/Version/1.0.0.md "1.0.0")

* [1.0.1](./Docs/Version/1.0.1.md "1.0.1")

* [1.0.4](./Docs/Version/1.0.1.md "1.0.1")